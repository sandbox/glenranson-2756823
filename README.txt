CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

eCard gives the user the ability to send a customized eCard.

The user fills out a form with details including recipient email address, which
image as a background for the eCard, a greeting, a message and whether or not to
receive a copy of the sent eCard.

eCard uses a separate font file for the text that is rendered on the eCard, but
could easily be altered to any font required (You will need to update the
module file where the font file is called).

It also has a separate page with the submission results of previously sent
eCards for tracking.

REQUIRMENTS
-----------

* Mimiemail (https://www.drupal.org/project/mimemail)
* Mail System (https://www.drupal.org/project/mailsystem)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
 https://drupal.org/documentation/install/modules-themes/modules-7
 for further information.

CONFIGURATION
-------------

This module ships with three sample PNG images. You will need to replace these
images with those of your choice (ensure they are of type PNG).
If you choose to use different names for images you will need to update the
template file, as the names are hard coded.

MAINTAINERS
-----------

Current Maintainers
* Glen Ranson (GlenRanson) - https://www.drupal.org/user/2627521
* Hayden Tapp (haydent) - https://drupal.org/user/2763191

<?php

/**
* @param $form
* @param $form_state
* @return mixed
*
* eCard settings for uploading new background images
*/
function ecard_settings($form, &$form_state) {

  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['ecard_images'] = array(
    '#type' => 'fieldset',
    '#title' => 'Background Images',
    '#group' => 'tabs',
    '#collapsible' => TRUE,
  );

  $form['ecard_images']['ecard_images_markup'] = array(
    '#markup' => t('<Strong>Background Images</strong><br>Use this page to 
upload new background images. <br>File names will be replaced on upload, as 
the template file references specific named files'),
  );

  $form['ecard_images']['ecard_upload_1'] = array(
    '#type' => 'managed_file',
    '#title' => t('Upload image 1'),
    '#description' => t('This will replace image 1'),
    '#default_value' => variable_get('ecard_upload_1', ''),
    '#upload_location' => 'public://ecards/',
    '#upload_validators' => array('file_validate_extensions' => array('png')),
  );

  $form['ecard_images']['ecard_upload_2'] = array(
    '#type' => 'managed_file',
    '#title' => t('Upload image 2'),
    '#description' => t('This will replace image 2'),
    '#default_value' => variable_get('ecard_upload_2', ''),
    '#upload_location' => 'public://ecards/',
    '#upload_validators' => array('file_validate_extensions' => array('png')),
  );

  $form['ecard_images']['ecard_upload_3'] = array(
    '#type' => 'managed_file',
    '#title' => t('Upload image 3'),
    '#description' => t('This will replace image 3'),
    '#default_value' => variable_get('ecard_upload_3', ''),
    '#upload_location' => 'public://ecards/',
    '#upload_validators' => array('file_validate_extensions' => array('png')),
  );

  $form['ecard_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Settings',
    '#group' => 'tabs',
    '#collapsible' => TRUE,
  );

  $form['ecard_settings']['ecard_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Font Color'),
    '#description' => t('Enter hexidecimal color code, without starting #
     ie;ffff4d'),
    '#default_value' => variable_get('ecard_text_color', 'ffff4d'),
  );

  $form['ecard_settings']['ecard_y_starting'] = array(
    '#type' => 'textfield',
    '#title' => t('Starting Y position'),
    '#description' => t('Enter Y starting point in pixels ie;100'),
    '#default_value' => variable_get('ecard_y_starting', '100'),
  );

  $form['ecard_settings']['ecard_x_starting'] = array(
    '#type' => 'textfield',
    '#title' => t('Starting X position'),
    '#description' => t('Enter Y starting point in pixels ie;100'),
    '#default_value' => variable_get('ecard_x_starting', '100'),
  );

  $form['ecard_settings']['ecard_text_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Wordwrap'),
    '#description' => t('Enter width of text area in pixels ie;100. 
    This is to produce a wordwrap effect'),
    '#default_value' => variable_get('ecard_text_width', '100'),
  );

  $form['ecard_settings']['ecard_text_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Font size'),
    '#description' => t('Enter font size ie;45'),
    '#default_value' => variable_get('ecard_text_size', '45'),
  );

  $form['ecard_settings']['ecard_text_line_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Line height'),
    '#description' => t('Enter line height ie;34. Works in conjuction with 
    wordwrap variable so may require a few attempts to get right'),
    '#default_value' => variable_get('ecard_text_line_height', '34'),
  );

  $form['ecard_upload_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return$form;
}

function ecard_settings_validate($form, &$form_state) {

  foreach ($form_state['values'] as $key => $fid) {
    if (!empty($fid)) {
      switch ($key) {

        case 'ecard_upload_1':
          // Load the file via file.fid.
          $file = file_load($fid);
          // Change status to permanent.
          $file->status = FILE_STATUS_PERMANENT;
          if ($file->filename != '1.png') {
            $file->filename = '1.png';
            $new_file = file_move($file, 'public://ecards/1.png', FILE_EXISTS_REPLACE);
            $form_state['values'][$key] = $new_file->fid;
          }
          else {
            file_save($file);
          }
          file_usage_add($file, 'ecard', 'ecard', $fid);

          break;

        case 'ecard_upload_2':
          // Load the file via file.fid.
          $file = file_load($fid);
          // Change status to permanent.
          $file->status = FILE_STATUS_PERMANENT;
          if ($file->filename != '2.png') {
            $file->filename = '2.png';
            $new_file = file_move($file, 'public://ecards/2.png', FILE_EXISTS_REPLACE);
            $form_state['values'][$key] = $new_file->fid;
          }
          else {
            $file = file_save($file);
          }
          file_usage_add($file, 'ecard', 'ecard', $fid);
          break;

        case 'ecard_upload_3':
          // Load the file via file.fid.
          $file = file_load($fid);
          // Change status to permanent.
          $file->status = FILE_STATUS_PERMANENT;
          if ($file->filename != '3.png') {
            $file->filename = '3.png';
            $new_file = file_move($file, 'public://ecards/3.png', FILE_EXISTS_REPLACE);
            $form_state['values'][$key] = $new_file->fid;
          }
          else {
            file_save($file);
          }
          file_usage_add($file, 'ecard', 'ecard', $fid);
          break;
      }
    }
  }

}

function ecard_settings_submit($form, &$form_state) {
  //Save background images
  variable_set('ecard_upload_1', $form_state['values']['ecard_upload_1']);
  variable_set('ecard_upload_2', $form_state['values']['ecard_upload_2']);
  variable_set('ecard_upload_3', $form_state['values']['ecard_upload_3']);
  //Save settings variables
  variable_set('ecard_text_color', $form_state['values']['ecard_text_color']);
  variable_set('ecard_y_starting', $form_state['values']['ecard_y_starting']);
  variable_set('ecard_x_starting', $form_state['values']['ecard_x_starting']);
  variable_set('ecard_text_size', $form_state['values']['ecard_text_size']);
  variable_set('ecard_text_width', $form_state['values']['ecard_text_width']);
  variable_set('ecard_text_line_height', $form_state['values']['ecard_text_line_height']);

  drupal_set_message('Settings saved');
}

(function ($) {
  
  $(document).ready(function() {
  if ($('input[name="ecard_image"]').val().length > 0) {
    var selected = $('input[name="ecard_image"]').val();
    var image = $('img[src="' + selected + '"]');
    image.parent().addClass('focus');
  }
  
  $('.img-container a').click(function() {
    $('a.focus').removeClass('focus');
    $(this).addClass('focus');
    $('input[name="ecard_image"]').val($(this).find('img').attr('src'));
  });  
 });

}(jQuery));
